import React from 'react';
import {Redirect,Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Cafe from "./containers/Cafe/Cafe";
import AddCafe from "./containers/Cafe/AddCafe";
import OneCafe from "./containers/Cafe/OneCafe/OneCafe";

const ProtectedRoute = ({isAllowed, ...props}) => {
    return isAllowed ? <Route {...props} /> : <Redirect to="/login"/>
};

const Routes = ({user})  => {
  return (
    <Switch>
        <Route path="/" exact component={Cafe}/>
        <Route path="/add" exact component={AddCafe}/>
      <Route path="/register" exact component={Register}/>
      <Route path="/login" exact component={Login}/>
        <Route path="/:id" exact component={OneCafe}/>
    </Switch>
  );
};

export default Routes;
