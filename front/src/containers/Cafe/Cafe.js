import React, {Component, Fragment} from 'react';
import {Button, Modal, ModalBody, ModalFooter} from 'reactstrap';
import {connect} from "react-redux";
import './Cafe.css';
import RecipeThumbnail from "../../components/CafeThumbnail/CafeThumbnail";
import ModalHeader from "reactstrap/es/ModalHeader";
import CafeListItem from "../../components/ListItems/CafeListItem";
import {deleteCafe, fetchCafe} from "../../store/actions/cafeActions";


class Cafe extends Component {
    state = {
        selectedItem: null
    };

    showModal = photo => {
        this.setState({selectedItem: photo})
    };

    hideModal = () => {
        this.setState({selectedItem: null})
    };

    componentDidMount() {
        this.props.fetchCafe();
    }

    render() {
        const cafe = this.props.cafe.map(cafe => (
            <CafeListItem
                showModal={() => this.showModal(cafe)}
                key={cafe._id}
                _id={cafe._id}
                title={cafe.title}
                image={cafe.image}
                user={cafe.user}
                userRole={this.props.user}
                deleteCafe={() => this.props.deleteCafe(cafe._id)}
            />
        ));
        return (
            <Fragment>
                <h2>
                    All Places
                </h2>
                <div className="Cafe">
                    {cafe}
                </div>

                <Modal isOpen={!!this.state.selectedItem} toggle={this.hideModal}>
                    {this.state.selectedItem && (
                        <Fragment>
                            <ModalHeader><h3>{this.state.selectedItem.title}</h3></ModalHeader>
                            <ModalBody>
                                <RecipeThumbnail image={this.state.selectedItem.image}/>
                                <p>{this.state.selectedItem.description}</p>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="secondary" onClick={this.hideModal}>Cancel</Button>
                            </ModalFooter>
                        </Fragment>
                    )}
                </Modal>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    cafe: state.cafe.cafe,
    user: state.users.user,

});

const mapDispatchToProps = dispatch => ({
    fetchCafe: () => dispatch(fetchCafe()),
    deleteCafe: (Id) => dispatch(deleteCafe(Id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Cafe);


