import React, {Component} from 'react';
import {connect} from "react-redux";
import {Col, Button, Form, FormGroup, Input, Label} from "reactstrap";
import {createCafe} from "../../store/actions/cafeActions";




class AddCafe extends Component {

    state = {
        title: '',
        image: null,
        description:''
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });

        this.props.createCafe(formData);

    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
            <h2>Add new cafe</h2>
        <FormGroup row>
        <Label sm={2} for="title">Title</Label>
            <Col sm={10}>
            <Input
        type="text" required
        name="title" id="title"
        placeholder="Enter cafe title"
        value={this.state.title}
        onChange={this.inputChangeHandler}
        />
        </Col>
        </FormGroup>
        <FormGroup row>
        <Label sm={2} for="image">Image</Label>
            <Col sm={10}>
            <Input
        type="file" required
        name="image" id="image"
        onChange={this.fileChangeHandler}
        />
        </Col>
        </FormGroup>
        <FormGroup row>
        <Label sm={2} for="description">Description</Label>
            <Col sm={10}>
            <Input
        type="textarea" required
        name="description" id="description"
        placeholder="Enter cafe description"
        value={this.state.description}
        onChange={this.inputChangeHandler}
        />
        </Col>
        </FormGroup>
        <FormGroup row>
        <Col sm={{offset:2, size: 10}}>
    <Button type="submit" color="primary">Create</Button>
            </Col>
            </FormGroup>
            </Form>
    );
    }
}

const mapDispatchToProps = dispatch => ({
    createCafe: data => dispatch(createCafe(data))
});

export default connect(null, mapDispatchToProps)(AddCafe);