import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import Slider from "react-slick";
import {
    Button, ButtonGroup,
    Col,
    Form,
    FormGroup,
    Input,
    Label
} from "reactstrap";
import {NotificationManager} from 'react-notifications';
import {
    addComment,
    addImages,
    deleteComment, deleteImage,
    fetchComments,
    fetchImages,
    getOneCafe
} from "../../../store/actions/cafeActions";
import CafeThumbnail from "../../../components/CafeThumbnail/CafeThumbnail";
import StarRatings from "react-star-ratings";
import '../Cafe.css';
import FormElement from "../../../components/UI/Form/FormElement";

import './slick.css';
import {apiURL} from '../../../constants';


class OneCafe extends Component {

    state = {
        activeIndex: 0,
        author: '',
        comment: '',
        images: [],
        quality: 0,
        service: 0,
        interior: 0,
    };

    componentDidMount() {
        this.props.getOneCafe(this.props.match.params.id);
        this.props.fetchComments(this.props.match.params.id);
        this.props.fetchImages(this.props.match.params.id);
    }

    submitFormHandler = event => {
        event.preventDefault();
        const data = {
            message: this.state.comment,
            quality: this.state.quality,
            service: this.state.service,
            interior: this.state.interior,
            cafeId: this.props.cafe._id,
            overageRatings: this.state.overageRatings
        };
        this.props.addComment(data, this.props.match.params.id)
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        const fileValidation = /(.*?)\.(jpg|bmp|jpeg|png|img)$/;
        if (event.target.value.match(fileValidation)) {
            this.setState({
                [event.target.name]: event.target.files
            })
        } else {
            NotificationManager.error('Unsupported format')
        }
    };

    submitFormHandlerImages = event => {
        event.preventDefault();

        const formData = new FormData();

        for (let i = 0; i < this.state.images.length; i++) {
            formData.append("images", this.state.images[i]);
        }
        formData.append("cafeId", this.props.cafe._id,);
        this.props.addImages(formData, this.props.match.params.id);

        console.log(formData);
    };

    changeRating = (newRating, name) => {
        if (name === 'quality') {
            this.setState({
                quality: newRating
            });
        } else if (name === 'service') {
            this.setState({
                service: newRating
            });
        } else if (name === 'interior') {
            this.setState({
                interior: newRating
            });
        }
    };

    render() {
        const commentForm = <div>
            <Form onSubmit={this.submitFormHandler}>
                <FormElement
                    propertyName="comment"
                    title="Enter your comment"
                    type="textarea"
                    value={this.state.comment}
                    onChange={this.inputChangeHandler}
                    placeholder="Enter comment"
                />
                <FormGroup row>
                    <Col sm={{offset: 3, size: 9}}>
                        <FormGroup row>
                            <Col>
                                <p> quality of food</p>
                                <StarRatings
                                    starDimension={'20px'}
                                    rating={this.state.quality}
                                    starRatedColor="orange"
                                    changeRating={this.changeRating}
                                    numberOfStars={5}
                                    name="quality"
                                />
                            </Col>
                            <Col>
                                <p>service quality</p>
                                <StarRatings
                                    starDimension={'20px'}
                                    rating={this.state.service}
                                    starRatedColor="orange"
                                    changeRating={this.changeRating}
                                    numberOfStars={5}
                                    name="service"
                                />
                            </Col>
                            <Col>
                                <p>interior</p>
                                <StarRatings
                                    starDimension={'20px'}
                                    rating={this.state.interior}
                                    starRatedColor="orange"
                                    changeRating={this.changeRating}
                                    numberOfStars={5}
                                    name="interior"
                                />
                            </Col>
                        </FormGroup>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col sm={{offset: 3, size: 9}}>
                        <Button type="submit" color="primary">
                            Add comment
                        </Button>
                    </Col>
                </FormGroup>
            </Form>
        </div>;
        const photosAddForm = <div>
            <Form onSubmit={this.submitFormHandlerImages}>
                <h2>Add images</h2>
                <FormGroup row>
                    <Label sm={2} for="images">Image</Label>
                    <Col sm={10}>
                        <Input
                            type="file" multiple required
                            name="images" id="images"
                            onChange={this.fileChangeHandler}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col sm={{offset: 2, size: 10}}>
                        <Button type="submit" color="primary">Create</Button>
                    </Col>
                </FormGroup>
            </Form>
        </div>;


        const settings = {
            dots: true,
            infinite: false,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
        };

        return (
            <Fragment>
                <div>
                    <h2 style={{margin: "0 auto"}}>{this.props.cafe.title}</h2>
                    <CafeThumbnail image={this.props.cafe.image}/>
                    <span>{this.props.cafe.description}</span>&nbsp;
                </div>
                <Fragment>
                    <h2>Gallery</h2>
                    <Slider {...settings}>
                        {this.props.images && this.props.images.map((image) => (
                                    <div key={image._id}>
                                        {this.props.user && this.props.user.role === 'admin' ? <ButtonGroup>
                                            <Button color="danger"
                                                    onClick={() => this.props.deleteImage(image._id,this.props.match.params.id)}>Delete</Button>
                                        </ButtonGroup> : null}
                                        <img src={`${apiURL}/uploads/${image.images}`} className="images"  alt="CafeImages"/>
                                    </div>
                        ))}
                    </Slider>
                </Fragment>
                <div>
                    <h3>Comments</h3>
                    {this.props.comments.length ? this.props.comments.map((comment, index) => {
                        return <div key={index}>
                            <span>{comment.data} </span><span>{comment.user}</span>
                            <p>{comment.message}</p>
                            <FormGroup>
                                <Col sm={4}>
                                    <span>Quality of food: </span>
                                    <StarRatings
                                        starDimension={'20px'}
                                        rating={comment.quality}
                                        starRatedColor="orange"
                                        numberOfStars={5}
                                    />
                                    <span> {comment.quality}.0</span>
                                </Col>
                                <Col sm={4}>
                                    <span>Service quality: </span>
                                    <StarRatings
                                        starDimension={'20px'}
                                        rating={comment.service}
                                        starRatedColor="orange"
                                        numberOfStars={5}
                                    />
                                    <span> {comment.service}.0</span>
                                </Col>
                                <Col sm={4}>
                                    <span>Interior:</span>
                                    <StarRatings
                                        starDimension={'20px'}
                                        rating={comment.interior}
                                        starRatedColor="orange"
                                        numberOfStars={5}
                                    />
                                    <span> {comment.interior}.0</span>
                                </Col>
                            </FormGroup>
                            {this.props.user && this.props.user.role === 'admin' ? <ButtonGroup>
                                <Button color="danger"
                                        onClick={() => this.props.deleteComment(comment._id, this.props.match.params.id)}>Delete</Button>
                            </ButtonGroup> : null}
                        </div>
                    }) : null}
                </div>
                {this.props.user ? <div> {commentForm} {photosAddForm} </div> : null}
            </Fragment>
    );
    }
    }

    const mapStateToProps = state => ({
        cafe: state.cafe.oneCafe,
        comments: state.cafe.comments,
        images: state.cafe.images,
        user: state.users.user,
    });

    const mapDispatchToProps = dispatch => ({
        getOneCafe: Id => dispatch(getOneCafe(Id)),
        addImages: (data,Id) => dispatch(addImages(data,Id)),
        addComment: (data, id) => dispatch(addComment(data, id)),
        fetchComments: (Id) => dispatch(fetchComments(Id)),
        fetchImages: (Id) => dispatch(fetchImages(Id)),
        deleteComment: (comId, Id) => dispatch(deleteComment(comId, Id)),
        deleteImage: (IdImage,Id) => dispatch(deleteImage(IdImage,Id)),

    });

    export default connect(mapStateToProps, mapDispatchToProps)(OneCafe);