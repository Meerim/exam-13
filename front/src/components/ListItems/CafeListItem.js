import React from 'react';
import PropTypes from 'prop-types';
import {Button, ButtonGroup, Card, CardBody} from "reactstrap";
import {Link} from "react-router-dom";
import '../../containers/Cafe/Cafe.css';
import CafeThumbnail from "../CafeThumbnail/CafeThumbnail";
import CardFooter from "reactstrap/es/CardFooter";


const CafeListItem = props => {
    return (
        <Card className="Cafe-card" >
            <CardBody style={{'marginTop': '10px', cursor: "pointer"}} onClick={()=>props.showModal(props)}>
                <CafeThumbnail image={props.image}/>
                <Link to={'/' + props._id}>
                    <h2>{props.title}</h2>
                </Link>
                <p>By:{props.user.username}</p>
            </CardBody>
            <CardFooter>
                { props.userRole && props.userRole.role === 'admin' ? <ButtonGroup>
                    <Button color="danger" onClick={props.deleteCafe}>Delete</Button>
                </ButtonGroup> : null}
            </CardFooter>
        </Card>
    );
};

CafeListItem.propTypes ={
    _id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    user: PropTypes.object,
    image: PropTypes.string,
    userRole: PropTypes.string,
    deleteCafe: PropTypes.func,
    showModal: PropTypes.func.isRequired

};
export default CafeListItem;