import axios from '../../axios-api';
import {push} from "connected-react-router";
import {NotificationManager} from 'react-notifications';


export const FETCH_CAFE_SUCCESS = 'FETCH_CAFE_SUCCESS';
export const FETCH_ONE_CAFE_SUCCESS = 'FETCH_ONE_CAFE_SUCCESS';
export const FETCH_COMMENTS_SUCCESS ='FETCH_COMMENT_SUCCESS';
export const CREATE_COMMENT_SUCCESS ='CREATE_COMMENT_SUCCESS';
export const FETCH_IMAGES ='FETCH_IMAGES';
export const CREATE_CAFE_SUCCESS = 'CREATE_CAFE_SUCCESS';
export const FETCH_FAIL = 'FETCH_FAIL';

export const fetchCafeSuccess = cafe => ({type: FETCH_CAFE_SUCCESS, cafe});
export const fetchOneCafeSuccess = oneCafe => ({type: FETCH_ONE_CAFE_SUCCESS, oneCafe});
export const fetchCommentsSuccess = comments =>({type: FETCH_COMMENTS_SUCCESS, comments});
export const fetchImagesSuccess = images =>({type: FETCH_IMAGES, images});
export const createCommentSuccess = () =>({type: CREATE_COMMENT_SUCCESS});
export const createCafeSuccess = () => ({type: CREATE_CAFE_SUCCESS});
export const fetchFail = error => ({type: FETCH_FAIL, error});

export const fetchCafe = () => {
    return (dispatch )=> {
            return axios.get('/cafe').then(
                response => {
                    dispatch(fetchCafeSuccess(response.data))
                }
            );
    };
};

export const createCafe = data => {
    return (dispatch, getState) => {

        const user = getState().users.user;
        if(user === null){
            dispatch(push('/login'));
        }else{
            return axios.post('/cafe', data).then(
                () => {
                    dispatch(createCafeSuccess());
                    dispatch(fetchCafe());
                    dispatch(push('/'));
                    NotificationManager.success('Your cafe  added');
                }
            );
        }

    };
};

export const getOneCafe = (Id) => {
    return dispatch => {
        return axios.get('/cafe/' + Id).then(
            response => {
                return dispatch(fetchOneCafeSuccess(response.data));
            },

        );
    }
};


export const deleteCafe = (Id) => {
    return (dispatch,getState) => {
        const user = getState().users.user;
        if(user === null){
            dispatch(push('/login'));
        }else{
            return axios.delete('/cafe/' + Id).then(
                () => {
                    dispatch(fetchCafe());
                    NotificationManager.success('Deleted cafe  successfully');
                },
                error => dispatch(fetchFail(error))
            );
        }

    }
};

export const fetchImages = Id =>{
    return dispatch =>{
        return axios.get('/images/'+ Id).then(
            response=> {
                return dispatch(fetchImagesSuccess(response.data))}
        )
    };
};

export const addImages = (data,Id) => {
    console.log(data);
    return dispatch => {
        return axios.post(`/images`, data).then(
            () => {
                dispatch(fetchImages(Id));
                NotificationManager.success('Images added')
            })
    }
};

export const deleteImage = (imageId, Id) => {
    return (dispatch,getState) => {
        const user = getState().users.user;
        if(user === null){
            dispatch(push('/login'));
        }else{
            return axios.delete('/images/' + imageId).then(
                () => {
                    dispatch(fetchImages(Id));
                    NotificationManager.success('Deleted image  successfully');
                },
                error => dispatch(fetchFail(error))
            );
        }

    }
};

export const fetchComments = comId =>{
    return dispatch =>{
        return axios.get('/comments/'+ comId).then(
            response=> {
                return dispatch(fetchCommentsSuccess(response.data))}
        );
    };
};


export const addComment = (data, id) => {
    return dispatch => {
        return axios.post(`/comments`, data).then(
            response => {
                dispatch(createCommentSuccess());
                dispatch(fetchComments(id));
                NotificationManager.success('Your comment added')
            })
    }
};

export const deleteComment = (comId, Id) => {
    return (dispatch,getState) => {
        const user = getState().users.user;
        if(user === null){
            dispatch(push('/login'));
        }else{
            return axios.delete('/comments/' + comId).then(
                () => {
                    dispatch(fetchComments(Id));
                    NotificationManager.success('Deleted comment  successfully');
                },
                error => dispatch(fetchFail(error))
            );
        }

    }
};





