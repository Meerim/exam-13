import {FETCH_CAFE_SUCCESS, FETCH_COMMENTS_SUCCESS, FETCH_IMAGES, FETCH_ONE_CAFE_SUCCESS} from "../actions/cafeActions";


const initialState = {
    cafe: [],
    oneCafe: "",
    comments: [],
    images:null
};

const cafeReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CAFE_SUCCESS:
            return {...state, cafe: action.cafe};
        case FETCH_ONE_CAFE_SUCCESS:
            return {...state, oneCafe: action.oneCafe};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, comments: action.comments};
        case FETCH_IMAGES:
            return {...state, images: action.images};
        default:
            return state;
    }
};

export default cafeReducer;