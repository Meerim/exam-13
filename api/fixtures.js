const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');

const Users = require('./models/User');
const Cafe = require('./models/Cafe');


const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const users = await Users.create({
        username: 'user',
        password: '123',
        role: 'user',
        token: nanoid()
    }, {
        username: "admin",
        password: '123',
        role: "admin",
        token: nanoid()
    });
    await Cafe.create(
        {
            user: users[0],
            title: 'Frisson Espresso',
            description: 'Small, family owned. Coffees are amazing. Baked goods are above average. Warm, clean. Small barstool setting seats 6-8 at best. Worth the walk from the awful on 7th Avenue.',
            image: 'Frisson.jpeg'
        },
        {
            user: users[0],
            title: 'Frisson Espresso',
            description: 'Small, family owned. Coffees are amazing. Baked goods are above average. Warm, clean. Small barstool setting seats 6-8 at best. Worth the walk from the awful on 7th Avenue.',
            image: 'Frisson.jpeg'
        },
        {
            user: users[0],
            title: 'Frisson Espresso',
            description: 'Small, family owned. Coffees are amazing. Baked goods are above average. Warm, clean. Small barstool setting seats 6-8 at best. Worth the walk from the awful on 7th Avenue.',
            image: 'Frisson.jpeg'
        },
    );

    return connection.close();
};


run().catch(error => {
    console.error('Something wrong happened...', error);
});
