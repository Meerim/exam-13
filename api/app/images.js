const express = require('express');
const Images = require('../models/Images');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const config = require('../config');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});
const router = express.Router();

router.get('/:id', async (req, res) => {
    try {
        const  criteria = {cafeId: req.params.id};
        const images = await Images.find(criteria).populate('cafeId');
        return res.send(images);
    } catch (e) {
        return res.status(500).send(e);
    }
});

router.post('/', [auth, upload.array('images', 10)], async (req, res) => {
    try {
        const images = {
            user: req.user._id,
            cafeId: req.body.cafeId,
            images: ''
        };
        for (let i = 0; i < req.files.length; i++) {
            images.images = req.files[i].filename;
            const CafeImages = new Images(images);
            await CafeImages.save();

        }
        return res.send(images);
    } catch (e) {
        if (e.name === 'ValidationError') {
            return res.status(400).send(e);
        }
        return res.status(500).send(e);
    }
});

router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    const image = await Images.findOne({_id: req.params.id});
    if (!image) {
        return res.sendStatus(403);
    }
    image.images.slice(req.body.index,1);
    await image.remove();
    res.send({message: "OK"})
});


module.exports = router;