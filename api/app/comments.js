const express = require('express');
const Comment = require('../models/Comment');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const router = express.Router();

router.get('/:id', async (req, res) => {
    try {
        const  criteria =  {cafeId: req.params.id};
        const comment = await Comment.find(criteria).populate('cafeId').sort({"data": -1});
        return res.send(comment);
    } catch (e) {
        return res.status(500).send(e);
    }
});

 router.post('/', [auth], async (req, res) => {
    try {
        const message = new Comment({
            data: new Date().toLocaleString("ru-RU"),
            user: req.user.username,
            message: req.body.message,
            quality: req.body.quality,
            service: req.body.service,
            interior: req.body.interior,
            cafeId: req.body.cafeId
        });
        await  message.save();
        return res.send(message);
    } catch (error) {
        console.log(error);
        return res.status(400).send(error)
    }
});

router.delete('/:id',[auth, permit('admin')], async (req, res) => {
    const comment = await Comment.findOne({_id: req.params.id});
    if (!comment) {
        return res.sendStatus(403);
    }
    await comment.remove();
    res.send({message: "OK"})
});


module.exports = router;
