const express = require('express');
const Cafe = require('../models/Cafe');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});
const router = express.Router();

router.get('/',  async (req, res) => {
    try {
        let criteria = {};

        if (req.query.user) {
            criteria = {
                user: req.query.user
            }
        }
        const cafe = await Cafe.find(criteria).populate('user');
        return res.send(cafe);
    } catch (e) {
        console.log(e);
        return res.status(500).send(e);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const cafe = await Cafe.findById(req.params.id).populate('user');
        return res.send(cafe)
    } catch (error) {
        return res.status(400).send(error)
    }
});

router.post('/', auth, upload.single('image'), async (req, res) => {
    try {
        const cafe = new Cafe({
            title: req.body.title,
            description: req.body.description,
            user: req.user._id,
            image: req.file.filename
        });

        await cafe.save();

        res.send(cafe);

    } catch (e) {
        if (e.name === 'ValidationError') {
            return res.status(400).send(e);
        }
        return res.status(500).send(e);
    }
});


router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    const cafe = await Cafe.findOne({ _id: req.params.id});

    if (!cafe) {
        return res.sendStatus(403);
    }
    await cafe.remove();
    res.send({message: "OK"})
});

module.exports = router;
