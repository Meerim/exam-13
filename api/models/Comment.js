const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CommentSchema = new Schema({
    message: {
        type: String
    },
    data: {
        type:Date
    },
    user: {
        type:String
    },
    quality: {
        type: Number
    },
    service: {
        type: Number
    },
    interior: {
        type: Number
    },
    cafeId: {
        type: Schema.Types.ObjectId,
        ref: 'Cafe',
        required: true
    },
});

const Comment = mongoose.model('Comment', CommentSchema);

module.exports = Comment;