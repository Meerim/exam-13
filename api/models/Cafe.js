const mongoose =require('mongoose');

const Schema = mongoose.Schema;

const CafeSchema= new Schema({
    title:{
        type:String,
        required: true,
    },
    image: String,
    images: Array,
    description:String,
    user: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    }
});

const  Cafe = mongoose.model('Cafe', CafeSchema);


module.exports = Cafe;