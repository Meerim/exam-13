const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ImagesSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    cafeId: {
        type: Schema.Types.ObjectId,
        ref: 'Cafe',
        required: true
    },
    images: {
        type: String
    },

});

const Images = mongoose.model('Images', ImagesSchema);

module.exports = Images;