const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  dbUrl: process.env.NODE_EVN === 'test' ? 'mongodb://localhost/cafe' : 'mongodb://localhost/cafe',
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  }
};
