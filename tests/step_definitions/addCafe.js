const {I} = inject();
// Add in your custom step files

When('нажимаю на кнопку и перехожу на ссылку создания кафе' , () => {
    I.waitForVisible(`//a[@class='dropdown-toggle nav-link']`, 10);
    I.click(`//a[@class='dropdown-toggle nav-link']`);
    I.click(`//a[contains(text(),'Add new cafe')]`);
    I.amOnPage('add')
});

When('я заполняю данные в форме создания кафе', () => {
    I.waitForVisible(`//h3[contains(text(),'Add new cafe')]`, 10);
    I.fillField(`//input[@id='title']`, "Frisson");
    I.attachFile(`//input[@id='image']`, 'file/Frisson.jpeg');
    I.fillField(`//textarea[@id='description']`, "Some description");
    I.click(`//button[@class='btn btn-primary']`);
});

Then('я вижу ответ от сервера {string}', text => {
    I.waitForText(text);
});

Then('я вижу ответ от сервера и такое заведение на странице', () => {
    I.waitForText("Your cafe added");
    I.waitForVisible(`//a[.='Frisson']`);
});
