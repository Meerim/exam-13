# language: ru

Функция: Добавление нового кафе

  @addCafe
  Сценарий: Добавить кафе
    Допустим я авторизованный пользователь "User"
    Если нажимаю на кнопку и перехожу на ссылку создания кафе
    И я заполняю данные в форме создания кафе
    То я вижу ответ от сервера и такое кафе на странице